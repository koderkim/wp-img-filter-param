<?php
	// SECURITY : Exit if accessed directly
	if(!defined('ABSPATH')){
		exit;
	}

	global $wpdb;
	$post = $wpdb->get_results("SELECT * FROM `".$wpdb->prefix."ifp_post`");
	$param_name = $wpdb->get_results("SELECT * FROM `".$wpdb->prefix."ifp_post_param_name`");
	$param = $wpdb->get_results("SELECT * FROM `".$wpdb->prefix."ifp_post_param` WHERE `param_id` = ".$param_name[0]->id);
	
	// $pName = array_filter($param_name, function($item){
	// 	return $item->id == 2;
	// });
	// var_dump(current($pName));
?>

<div class="wrap">
	<form method="post" action="options.php">
		<?php //settings_fields('wp-sitemap-page');?>
		<input type="hidden" name="param-name" value='<?php echo json_encode($param_name); ?>'>
		<div id="icon-options-general" class="icon32"></div>
		<h2>WP Image Filter Param</h2>
		<div id="poststuff">
			<div id="post-body">
				<div id="post-body-content">
					<div class="postbox">
						<h3 class="hndle"><span>Хараткеристики</span></h3>
						<div class="inside">
							<div>
								<div class="inline-box">
									<div>Свойство:</div>
									<select name="param-name">
									<?php foreach($param_name as $row): ?>
										<option value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option>
									<?php endforeach; ?>
									</select>
									<div id="add-param-name" class="add-box">
										<a href="#" class="add-link">+&nbsp;добавить</a>
										<div class="add-form">
											<input type="text" name="input-param-name">
											<div class="add-form-btn">
												<a href="#" class="add-btn">Добавить</a>
												<a href="#" class="add-close">Отмена</a>
											</div>
										</div>
									</div>
								</div>
								<div class="inline-box">
									<div>Значение:</div>
									<select name="param-value">
									<?php foreach($param as $row): ?>
										<option value="<?php echo $row->id; ?>"><?php echo $row->value; ?></option>
									<?php endforeach; ?>
									</select>
									<div id="add-param-value" class="add-box">
										<a href="#" class="add-link">+&nbsp;добавить</a>
										<div class="add-form">
											<input type="text" name="input-param-value">
											<div class="add-form-btn">
												<a href="#" class="add-btn">Добавить</a>
												<a href="#" class="add-close">Отмена</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div><!-- .inside -->
					</div>
					<div class="postbox">
						<h3 class="hndle"><span><?php _e('Posts', 'wp-img-filter-param'); ?></span></h3>
						<div class="inside">
							<div id="ifp-post-wrapper">
								<div id="ifp-post-add">
									<a href="#" title="<?php _e('add post', 'wp-img-filter-param'); ?>">
										<svg style="width:24px;height:24px;vertical-align:top" viewBox="0 0 24 24">
										    <path fill="#000000" d="M12,20C7.59,20 4,16.41 4,12C4,7.59 7.59,4 12,4C16.41,4 20,7.59 20,12C20,16.41 16.41,20 12,20M12,2A10,10 0 0,0 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2M13,7H11V11H7V13H11V17H13V13H17V11H13V7Z" />
										</svg>&nbsp;<?php _e('Add post', 'wp-img-filter-param'); ?>
									</a>
								</div>
							<?php foreach($post as $row): ?>
								<div class="ifp-post" data-id="<?php echo $row->id; ?>">
									<div class="ifp-post-name">
										<span><?php echo $row->name; ?></span>
										<div class="ifp-post-name-tools">
											<input type="text" disabled value="<?php echo '[wp_ifp id=&quot;'.trim($row->id).'&quot;]'; ?>">
											<a href="#" class="del" title="<?php _e('Delete', 'wp-img-filter-param'); ?>">
												<svg style="width:18px;height:18px;vertical-align:middle" viewBox="0 0 24 24">
												    <path fill="#666666" d="M6,19A2,2 0 0,0 8,21H16A2,2 0 0,0 18,19V7H6V19M8.46,11.88L9.87,10.47L12,12.59L14.12,10.47L15.53,11.88L13.41,14L15.53,16.12L14.12,17.53L12,15.41L9.88,17.53L8.47,16.12L10.59,14L8.46,11.88M15.5,4L14.5,3H9.5L8.5,4H5V6H19V4H15.5Z" />
												</svg>
											</a>
											<a href="#" class="add-img" title="<?php _e('add photo', 'wp-img-filter-param'); ?>">
												<svg style="width:24px;height:24px;vertical-align:middle" viewBox="0 0 24 24">
													<path fill="#666666" d="M19,11H15V15H13V11H9V9H13V5H15V9H19M20,2H8A2,2 0 0,0 6,4V16A2,2 0 0,0 8,18H20A2,2 0 0,0 22,16V4A2,2 0 0,0 20,2M4,6H2V20A2,2 0 0,0 4,22H18V20H4V6Z" />
												</svg><?php _e('Add image', 'wp-img-filter-param'); ?>
											</a>
											<a href="#" class="edit" title="<?php _e('edit', 'wp-img-filter-param'); ?>">
												<svg style="width:24px;height:24px;vertical-align:middle" viewBox="0 0 24 24">
												    <path fill="#666666" d="M19,20H4C2.89,20 2,19.1 2,18V6C2,4.89 2.89,4 4,4H10L12,6H19A2,2 0 0,1 21,8H21L4,8V18L6.14,10H23.21L20.93,18.5C20.7,19.37 19.92,20 19,20Z" />
												</svg><?php _e('Edit', 'wp-img-filter-param'); ?>
											</a>
										</div>
									</div>
									<div class="ifp-post-box">
										
									</div>
								</div>
							<?php endforeach; ?>
								<div class="ifp-post ifp-post-template" data-id="">
									<div class="ifp-post-name">
										<span><?php _e('New post', 'wp-img-filter-param'); ?></span>
										<div class="ifp-post-name-tools">
											<a href="#" class="del" title="<?php _e('Delete', 'wp-img-filter-param'); ?>">
												<svg style="width:18px;height:18px;vertical-align:middle" viewBox="0 0 24 24">
												    <path fill="#666666" d="M6,19A2,2 0 0,0 8,21H16A2,2 0 0,0 18,19V7H6V19M8.46,11.88L9.87,10.47L12,12.59L14.12,10.47L15.53,11.88L13.41,14L15.53,16.12L14.12,17.53L12,15.41L9.88,17.53L8.47,16.12L10.59,14L8.46,11.88M15.5,4L14.5,3H9.5L8.5,4H5V6H19V4H15.5Z" />
												</svg>
											</a>
											<a href="#" class="add-img" title="<?php _e('Add photo', 'wp-img-filter-param'); ?>">
												<svg style="width:24px;height:24px;vertical-align:middle" viewBox="0 0 24 24">
													<path fill="#666666" d="M19,11H15V15H13V11H9V9H13V5H15V9H19M20,2H8A2,2 0 0,0 6,4V16A2,2 0 0,0 8,18H20A2,2 0 0,0 22,16V4A2,2 0 0,0 20,2M4,6H2V20A2,2 0 0,0 4,22H18V20H4V6Z" />
												</svg><?php _e('Add image', 'wp-img-filter-param'); ?>
											</a>
											<a href="#" class="edit" title="<?php _e('Edit', 'wp-img-filter-param'); ?>">
												<svg style="width:24px;height:24px;vertical-align:middle" viewBox="0 0 24 24">
												    <path fill="#666666" d="M19,20H4C2.89,20 2,19.1 2,18V6C2,4.89 2.89,4 4,4H10L12,6H19A2,2 0 0,1 21,8H21L4,8V18L6.14,10H23.21L20.93,18.5C20.7,19.37 19.92,20 19,20Z" />
												</svg><?php _e('Edit', 'wp-img-filter-param'); ?>
											</a>
										</div>
									</div>
									<div class="ifp-post-box">
										
									</div>
								</div>
							</div>
						</div><!-- .inside -->
					</div>
				</div>
			</div>
		</div>
	</form>
</div>

<script>
	/* LOADER */
	loader = {};
	loader.show = function(){
		jQuery('body').append('<div id="loader-box"><img src="<?php echo plugins_url( '/images/loader.svg', __FILE__); ?>" alt="loading..." /></div>');
	}
	loader.hide = function(){
		jQuery('#loader-box').remove();
	}
	/* END LOADER */
</script>