function getParamName(id){
	var list = JSON.parse(jQuery('input[name="param-name"]').val());
	var res;
	list.forEach(function(item, i){
		if(item.id == id){
			res = item;
			// console.log(item);
		}
	});
	return res;
}
var frame, ifpPostTemplate, prevName, priceChange, descriptionChange;
jQuery(document).ready(function(){
	/* template html */
	ifpPostTemplate = jQuery('.ifp-post-template').clone().removeClass('ifp-post-template');
	jQuery('.ifp-post-template').remove();
	/* END template html */

	jQuery('.add-box a.add-link').on('click', function(){
		jQuery(this).closest('.add-box').addClass('show');
		jQuery(this).closest('.add-box').find('.add-form input').val('').focus();
		return false;
	});
	jQuery('.add-box a.add-close').on('click', function(){
		jQuery(this).closest('.add-box').removeClass('show');
		return false;
	});

	// param-name
	jQuery('#add-param-name .add-form a.add-btn').on('click', function(){
		loader.show();
		var addBox = jQuery(this).closest('.add-box');
		var name = addBox.find('.add-form input').val();
		var data = {
			'action': 'ifp_param_name_add',
			'name': name
		};
		jQuery.post(ajaxurl, data, function(response){
			addBox.removeClass('show');
			if(!response){
				loader.hide();
				alert('Ошибка');
				return false;
			}
			jQuery.post(ajaxurl, {'action': 'ifp_param_name_get'}, function(response){
				jQuery('select[name=param-name]').empty().append(response);
				loader.hide();
			});
		});
	});
	jQuery('select[name=param-name]').on('change', function(){
		loader.show();
		var data = {
			'action': 'ifp_param_get',
			'param_id': jQuery(this).val()
		};
		jQuery.post(ajaxurl, data, function(response){
			jQuery('select[name=param-value]').empty().append(response);
			loader.hide();
		});
	});

	// param-value
	jQuery('#add-param-value .add-form a.add-btn').on('click', function(){
		loader.show();
		var addBox = jQuery(this).closest('.add-box');
		var value = addBox.find('.add-form input').val();
		var data = {
			'action': 'ifp_param_add',
			'value': value,
			'param_id': jQuery('select[name=param-name]').val()
		};
		jQuery.post(ajaxurl, data, function(response){
			addBox.removeClass('show');
			if(!response){
				loader.hide();
				alert('Ошибка');
				return false;
			}
			jQuery.post(ajaxurl, {'action': 'ifp_param_get'}, function(response){
				jQuery('select[name=param-value]').empty().append(response);
				loader.hide();
			});
		});
	});

	// POST IMG
	jQuery('#ifp-post-add a').on('click', function(){
		jQuery('#ifp-post-wrapper').append(ifpPostTemplate.clone()).find('.ifp-post').last().find('.ifp-post-name span').trigger('click');
		return false;
	});
	jQuery('body').on('click', '.ifp-post .ifp-post-name span', function(){
		jQuery(this).addClass('edited').get(0).setAttribute('contenteditable', true);
		jQuery(this).focus();
		prevName = jQuery(this).text();
	}).on('blur', '.ifp-post .ifp-post-name span', function(){
		jQuery(this).removeClass('edited').get(0).setAttribute('contenteditable', false);
		var id = jQuery(this).closest('.ifp-post').data('id');
		var name = jQuery(this).text().trim();
		if(!id || prevName != jQuery(this).text()){
			loader.show();
			var data = {
				'action': 'ifp_post_add',
				'id': id ? id : 0,
				'name': name
			};
			jQuery.post(ajaxurl, data, function(response){
				if(!id){
					location.reload();
				} else {
					loader.hide();
				}
			});
		}
	});
	jQuery('body').on('click', '.ifp-post .ifp-post-name a.edit', function(e){
		var ifp_post = jQuery(this).closest('.ifp-post');
		ifp_post.find('.ifp-post-box').empty();
		if(!ifp_post.hasClass('show')){
			loader.show();
			var data = {
				'action': 'ifp_post_img_get',
				'echo': true,
				'post_id': ifp_post.data('id')
			};
			jQuery.post(ajaxurl, data, function(response){
				var list = JSON.parse(response);
				var html = '';
				for(item in list){
					html += '<div class="ifp-post-img" data-id="'+ list[item].id +'" data-img-id="'+ list[item].img_id +'">';
					html += '<div class="ifp-post-move"></div>';
					html += list[item].img;
					html += '<a href="#" class="ifp-post-img-del"><svg style="width:24px;height:24px" viewBox="0 0 24 24"><path fill="#666666" d="M6,19A2,2 0 0,0 8,21H16A2,2 0 0,0 18,19V7H6V19M8.46,11.88L9.87,10.47L12,12.59L14.12,10.47L15.53,11.88L13.41,14L15.53,16.12L14.12,17.53L12,15.41L9.88,17.53L8.47,16.12L10.59,14L8.46,11.88M15.5,4L14.5,3H9.5L8.5,4H5V6H19V4H15.5Z"></path></svg></a>';
					html += '<div class="ifp-post-img-description"><textarea>'+list[item].description+'</textarea></div>';
					html += '<div class="ifp-post-img-price"><label>'+langs.Price+':</label> <input type="text" value="'+list[item].price+'" /></div>';
					html += '<div class="ifp-post-img-add-param"><a href="#" class="visible"><svg style="width:18px;height:18px;vertical-align:top" viewBox="0 0 24 24"><path fill="#000000" d="M12,20C7.59,20 4,16.41 4,12C4,7.59 7.59,4 12,4C16.41,4 20,7.59 20,12C20,16.41 16.41,20 12,20M12,2A10,10 0 0,0 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2M13,7H11V11H7V13H11V17H13V13H17V11H13V7Z" /></svg>&nbsp;'+langs.Add_param+'</a></div>';
					for(i in list[item].params){
						$param = getParamName(list[item].params[i].param_id);
						html += '<div class="ifp-post-img-param" data-param-id="'+list[item].params[i].id+'" data-param-name-id="'+$param.id+'">';
						html += '<span>'+$param.name+'</span>: <span>'+list[item].params[i].value+'</span>';
						html += '<a href="#" class="edit-ifp-post-img-param"><svg style="width:18px;height:18px" viewBox="0 0 24 24"><path fill="#666666" d="M5,3C3.89,3 3,3.89 3,5V19A2,2 0 0,0 5,21H19A2,2 0 0,0 21,19V12H19V19H5V5H12V3H5M17.78,4C17.61,4 17.43,4.07 17.3,4.2L16.08,5.41L18.58,7.91L19.8,6.7C20.06,6.44 20.06,6 19.8,5.75L18.25,4.2C18.12,4.07 17.95,4 17.78,4M15.37,6.12L8,13.5V16H10.5L17.87,8.62L15.37,6.12Z" /></svg></a>';
						html += '<a href="#" class="del-ifp-post-img-param"><svg style="width:18px;height:18px" viewBox="0 0 24 24"><path fill="#666666" d="M6,19A2,2 0 0,0 8,21H16A2,2 0 0,0 18,19V7H6V19M8.46,11.88L9.87,10.47L12,12.59L14.12,10.47L15.53,11.88L13.41,14L15.53,16.12L14.12,17.53L12,15.41L9.88,17.53L8.47,16.12L10.59,14L8.46,11.88M15.5,4L14.5,3H9.5L8.5,4H5V6H19V4H15.5Z" /></svg></a>';
						html += '</div>';
					}
					html += '</div>';
				};
				ifp_post.find('.ifp-post-box').append(html);
				ifp_post.addClass('show');
				loader.hide();
			});
		} else {
			ifp_post.removeClass('show');
		}
		return false;
	});
	jQuery('body').on('click', '.ifp-post .ifp-post-name a.add-img', function(e){
		e.preventDefault();
		var box = jQuery(this).closest('.ifp-post');
		if(frame){
			frame.open();
			return;
	    }
		frame = wp.media({
			title: 'Select or Upload Photo',
			button: {text: 'Use this media'},
			multiple: true
	    });
		frame.on('select', function(){
			var data = {
				'action': 'ifp_post_img_add',
				'post_id': box.data('id'),
				'items': {}
			};
			var list = frame.state().get('selection').toJSON();
			var items = [];
			list.forEach(function(item, i, list){
				var html = '';
				html += '<div class="ifp-post-img" data-img-id="'+ item.id +'">';
				html += '<div class="ifp-post-move"></div>';
				html += '<img src="'+ item.sizes.thumbnail.url +'" width="'+ item.sizes.thumbnail.width +'" height="'+ item.sizes.thumbnail.height +'" alt="">';
				html += '<a href="#" class="ifp-post-img-del"><svg style="width:24px;height:24px" viewBox="0 0 24 24"><path fill="#666666" d="M6,19A2,2 0 0,0 8,21H16A2,2 0 0,0 18,19V7H6V19M8.46,11.88L9.87,10.47L12,12.59L14.12,10.47L15.53,11.88L13.41,14L15.53,16.12L14.12,17.53L12,15.41L9.88,17.53L8.47,16.12L10.59,14L8.46,11.88M15.5,4L14.5,3H9.5L8.5,4H5V6H19V4H15.5Z"></path></svg></a>';
				html += '<div class="ifp-post-img-description"><textarea></textarea></div>';
				html += '<div class="ifp-post-img-price"><label>'+langs.Price+'</label><input type="text" value="" /></div>';
				html += '<div class="ifp-post-img-add-param"><a href="#" class="visible"><svg style="width:18px;height:18px;vertical-align:top" viewBox="0 0 24 24"><path fill="#000000" d="M12,20C7.59,20 4,16.41 4,12C4,7.59 7.59,4 12,4C16.41,4 20,7.59 20,12C20,16.41 16.41,20 12,20M12,2A10,10 0 0,0 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2M13,7H11V11H7V13H11V17H13V13H17V11H13V7Z" /></svg>&nbsp;'+langs.Add_param+'</a></div>';
				html += '</div>';
				box.find('.ifp-post-box').append(html);
				items.push(item.id);
			});
			data.items = JSON.stringify(items);
			jQuery.post(ajaxurl, data, function(response){
				if(response){
					box.find('.ifp-post-box .ifp-post-img:last').attr('data-id', response);
				}
				loader.hide();
			});
		});
		frame.open();
	});
	jQuery('body').on('click', '.ifp-post .ifp-post-name a.del', function(e){
		loader.show();
		var postBox = jQuery(this).closest('.ifp-post');
		var postId = jQuery(this).closest('.ifp-post').attr('data-id');
		var data = {
			'action': 'ifp_post_delete',
			'id': postId,
		};
		jQuery.post(ajaxurl, data, function(response){
			postBox.remove();
			loader.hide();
		});
		return false;
	});
	jQuery('body').on('click', '.ifp-post .ifp-post-box .ifp-post-img a.ifp-post-img-del', function(){
		loader.show();
		var imgBox = jQuery(this).closest('.ifp-post-img');
		var id = imgBox.attr('data-id');
		var data = {
			'action': 'ifp_post_img_delete',
			'id': id
		};
		jQuery.post(ajaxurl, data, function(response){
			imgBox.remove();
			loader.hide();
		});
		return false;
	});
	jQuery('body').on('click', '.ifp-post .ifp-post-img-param a.del-ifp-post-img-param', function(e){
		var imgBox = jQuery(this).closest('.ifp-post-img');
		var id = imgBox.data('id');
		jQuery(this).closest('.ifp-post-img-param').remove();
		var param = [];
		imgBox.find('.ifp-post-img-param').each(function(){
			param.push(parseInt(jQuery(this).attr('data-param-id')));
		});
		loader.show();
		var data = {
			'action': 'ifp_post_img_update',
			'id': id,
			'param': param
		};
		jQuery.post(ajaxurl, data, function(response){
			loader.hide();
		});
		return false;
	});
	jQuery('body').on('click', '.ifp-post .ifp-post-img-param a.edit-ifp-post-img-param', function(){
		loader.show();
		var paramBox = jQuery(this).closest('.ifp-post-img-param');
		var data = {
			'action': 'ifp_param_get',
			'param_id': paramBox.attr('data-param-name-id')
		};
		jQuery.post(ajaxurl, data, function(response){
			var html = jQuery('<select></select>').append(response);
			paramBox.find('span:last').empty().append(html);
			paramBox.find('span select option[value='+paramBox.attr('data-param-id')+']').attr('selected', true);
			loader.hide();
		});
		return false;
	});
	jQuery('body').on('change', '.ifp-post .ifp-post-img-param span select', function(e){
		var imgBox = jQuery(this).closest('.ifp-post-img');
		var paramId = jQuery(this).val();
		var paramName = this.options[this.selectedIndex].text;
		jQuery(this).closest('.ifp-post-img-param').attr('data-param-id', paramId);
		jQuery(this).closest('span').empty().text(paramName);
		var id = imgBox.data('id');
		var param = [];
		imgBox.find('.ifp-post-img-param').each(function(){
			param.push(parseInt(jQuery(this).attr('data-param-id')));
		});
		loader.show();
		var data = {
			'action': 'ifp_post_img_update',
			'id': id,
			'param': param
		};
		jQuery.post(ajaxurl, data, function(response){
			loader.hide();
		});
		return false;
	});
	jQuery('body').on('click', '.ifp-post .ifp-post-img-add-param a.visible', function(){
		var addParamBox = jQuery(this).closest('.ifp-post-img-add-param');
		loader.show();
		jQuery.post(ajaxurl, {'action': 'ifp_param_name_get'}, function(response){
			var html = jQuery('<select name="param-name"></select>').append('<option value="0">-------------</option>').append(response);
			addParamBox.append('<div></div>');
			addParamBox.find('div').append(html);
			addParamBox.find('div').append('<a href="" class="cancel">'+langs.Cancel+'</a>');
			loader.hide();
		});
		return false;
	});
	jQuery('body').on('change', '.ifp-post .ifp-post-img-add-param select[name="param-name"]', function(){
		var addParamBox = jQuery(this).closest('.ifp-post-img-add-param');
		var paramId = jQuery(this).val();
		loader.show();
		var data = {
			'action': 'ifp_param_get',
			'param_id': paramId
		};
		jQuery.post(ajaxurl, data, function(response){
			if(addParamBox.find('select[name="param"]').length == 0){
				var html = jQuery('<select name="param"></select>').append(response);
				addParamBox.find('div').append(html);
				addParamBox.find('div').find('a').remove();
				addParamBox.find('div').append('<a href="#" class="add">'+langs.Add+'</a>').append('<a href="" class="cancel">'+langs.Cancel+'</a>');
			} else {
				addParamBox.find('div select[name="param"]').empty().append(response);
			}
			loader.hide();
		});
	});
	jQuery('body').on('click', '.ifp-post .ifp-post-img-add-param div a.cancel', function(){
		jQuery(this).closest('div').remove();
		return false;
	});
	jQuery('body').on('click', '.ifp-post .ifp-post-img-add-param div a.add', function(){
		var paramId = jQuery(this).closest('div').find('select[name="param"]').val();
		var paramText = jQuery(this).closest('div').find('select[name="param"] option:selected').text();
		var paramNameId = jQuery(this).closest('div').find('select[name="param-name"]').val();
		var paramNameText = jQuery(this).closest('div').find('select[name="param-name"] option:selected').text();
		html = '';
		html += '<div class="ifp-post-img-param" data-param-id="'+paramId+'" data-param-name-id="'+paramNameId+'">';
		html += '<span>'+paramNameText+'</span>: <span>'+paramText+'</span>';
		html += '<a href="#" class="edit-ifp-post-img-param"><svg style="width:18px;height:18px" viewBox="0 0 24 24"><path fill="#666666" d="M5,3C3.89,3 3,3.89 3,5V19A2,2 0 0,0 5,21H19A2,2 0 0,0 21,19V12H19V19H5V5H12V3H5M17.78,4C17.61,4 17.43,4.07 17.3,4.2L16.08,5.41L18.58,7.91L19.8,6.7C20.06,6.44 20.06,6 19.8,5.75L18.25,4.2C18.12,4.07 17.95,4 17.78,4M15.37,6.12L8,13.5V16H10.5L17.87,8.62L15.37,6.12Z" /></svg></a>';
		html += '<a href="#" class="del-ifp-post-img-param"><svg style="width:18px;height:18px" viewBox="0 0 24 24"><path fill="#666666" d="M6,19A2,2 0 0,0 8,21H16A2,2 0 0,0 18,19V7H6V19M8.46,11.88L9.87,10.47L12,12.59L14.12,10.47L15.53,11.88L13.41,14L15.53,16.12L14.12,17.53L12,15.41L9.88,17.53L8.47,16.12L10.59,14L8.46,11.88M15.5,4L14.5,3H9.5L8.5,4H5V6H19V4H15.5Z" /></svg></a>';
		html += '</div>';
		jQuery(this).closest('.ifp-post-img').append(html);
		var imgBox = jQuery(this).closest('.ifp-post-img');
		var id = imgBox.data('id');
		var param = [];
		imgBox.find('.ifp-post-img-param').each(function(){
			param.push(parseInt(jQuery(this).attr('data-param-id')));
		});
		loader.show();
		var data = {
			'action': 'ifp_post_img_update',
			'id': id,
			'param': param
		};
		jQuery.post(ajaxurl, data, function(response){
			loader.hide();
		});
		jQuery(this).closest('div').remove();
		return false;
	});
	jQuery('body').on('focus', '.ifp-post .ifp-post-box .ifp-post-img .ifp-post-img-description textarea', function(){
		descriptionChange = jQuery(this).val();
	});
	jQuery('body').on('blur', '.ifp-post .ifp-post-box .ifp-post-img .ifp-post-img-description textarea', function(){
		if(descriptionChange != jQuery(this).val().trim()){
			var imgBox = jQuery(this).closest('.ifp-post-img');
			var id = imgBox.data('id');
			loader.show();
			var data = {
				'action': 'ifp_post_img_update_description',
				'id': id,
				'description': jQuery(this).val().trim()
			};
			jQuery.post(ajaxurl, data, function(response){
				loader.hide();
			});
		}
	});
	jQuery('body').on('focus', '.ifp-post .ifp-post-box .ifp-post-img .ifp-post-img-price input', function(){
		priceChange = jQuery(this).val();
	});
	jQuery('body').on('blur', '.ifp-post .ifp-post-box .ifp-post-img .ifp-post-img-price input', function(){
		if(priceChange != jQuery(this).val().trim()){
			var imgBox = jQuery(this).closest('.ifp-post-img');
			var id = imgBox.data('id');
			loader.show();
			var data = {
				'action': 'ifp_post_img_update_price',
				'id': id,
				'price': jQuery(this).val().trim()
			};
			jQuery.post(ajaxurl, data, function(response){
				loader.hide();
			});
		}
	});

	// gallery image sort
	jQuery('.ifp-post .ifp-post-box').sortable({
		handle: '.ifp-post-move',
		stop: function(event, ui){
			var sortList = [];
			ui.item.closest('.ifp-post-box').find('.ifp-post-img').each(function(){
				sortList.push($(this).attr('data-id'));
			});
			loader.show();
			var data = {
				'action': 'ifp_post_img_sort',
				'sortList': JSON.stringify(sortList)
			};
			jQuery.post(ajaxurl, data, function(response){
				loader.hide();
			});
		}
	});
});