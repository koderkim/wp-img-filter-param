function ifpImgPrev(){
	var ifpImgPreviewId = jQuery('#ifp-img-preview').attr('data-id');
	var ifpImgPreviewImgId = jQuery('#ifp-img-preview').find('.ifp-img-photo').attr('data-id');
	var wrap = jQuery('.ifp-wrapper[data-id="'+ifpImgPreviewId+'"]');
	jQuery('#ifp-img-preview').remove();
	wrap.find('.ifp-img-box:not(.main)').removeClass('active');
	var prevElem = wrap.find('.ifp-img-box-items .ifp-img-box[data-img-id="'+ifpImgPreviewImgId+'"]').prev('.show').addClass('active');
	if(!prevElem.length){
		prevElem = wrap.find('.ifp-img-box-items .ifp-img-box.show:last').addClass('active');
	}
	wrap.find('.ifp-img-box.main a').attr('href', prevElem.find('a').attr('href'));
	wrap.find('.ifp-img-box.main a img').attr('src', prevElem.find('img').attr('src'));
	wrap.find('.ifp-img-box.main span').empty().text(prevElem.find('span').text());
	wrap.find('.ifp-img-description').empty().text(prevElem.find('span').text());
	wrap.find('.ifp-param .ifp-img-price span').empty().text(prevElem.attr('data-price'));
	wrap.find('.ifp-img-box.main').attr('data-param-id', prevElem.attr('data-param-id'));
	wrap.find('.ifp-img-box.main').attr('data-img-id', prevElem.attr('data-img-id'));
	wrap.find('.ifp-img-box.main').attr('data-price', prevElem.attr('data-price'));
	wrap.find('.ifp-img-box.main a').trigger('click');
}

function ifpImgNext(){
	var ifpImgPreviewId = jQuery('#ifp-img-preview').attr('data-id');
	var ifpImgPreviewImgId = jQuery('#ifp-img-preview').find('.ifp-img-photo').attr('data-id');
	var wrap = jQuery('.ifp-wrapper[data-id="'+ifpImgPreviewId+'"]');
	jQuery('#ifp-img-preview').remove();
	wrap.find('.ifp-img-box:not(.main)').removeClass('active');
	var nextElem = wrap.find('.ifp-img-box-items .ifp-img-box[data-img-id="'+ifpImgPreviewImgId+'"]').next('.show').addClass('active');
	if(!nextElem.length){
		nextElem = wrap.find('.ifp-img-box-items .ifp-img-box.show:first').addClass('active');
	}
	wrap.find('.ifp-img-box.main a').attr('href', nextElem.find('a').attr('href'));
	wrap.find('.ifp-img-box.main a img').attr('src', nextElem.find('img').attr('src'));
	wrap.find('.ifp-img-box.main span').empty().text(nextElem.find('span').text());
	wrap.find('.ifp-img-description').empty().text(nextElem.find('span').text());
	wrap.find('.ifp-param .ifp-img-price span').empty().text(nextElem.attr('data-price'));
	wrap.find('.ifp-img-box.main').attr('data-param-id', nextElem.attr('data-param-id'));
	wrap.find('.ifp-img-box.main').attr('data-img-id', nextElem.attr('data-img-id'));
	wrap.find('.ifp-img-box.main').attr('data-price', nextElem.attr('data-price'));
	wrap.find('.ifp-img-box.main a').trigger('click');
}

jQuery(document).ready(function(){
	jQuery('body').on('click', '.ifp-wrapper .ifp-param-box span', function(e){
		var span = jQuery(this);
		var paramBox = jQuery(this).closest('.ifp-param');
		var wrap = jQuery(this).closest('.ifp-wrapper');
		jQuery(this).closest('.ifp-param-box').find('span.selected').not(jQuery(this)).removeClass('selected');
		if(span.hasClass('selected')){
			span.removeClass('selected');
		} else {
			span.removeClass('unselected').addClass('selected');
		}
		if(paramBox.find('span.selected').length){
			paramBox.find('span:not(.selected)').addClass('unselected');
			var listId = [];
			paramBox.find('span.selected').each(function(){
				listId.push(jQuery(this).attr('data-id'));
			});
			paramBox.closest('.ifp-wrapper').find('.ifp-img-box-items .ifp-img-box').each(function(){
				var elem = jQuery(this);
				jQuery(this).removeClass('show');
				if(jQuery(this).attr('data-param-id').length){
					var paramId = JSON.parse(jQuery(this).attr('data-param-id'));
					var show = true;
					listId.forEach(function(item, i){
						if(jQuery.inArray(item.toString(), paramId) == -1){
							show = false;
						}
					});
					if(show) elem.addClass('show');
				}
			});
		} else {
			paramBox.find('span').removeClass('unselected');
			paramBox.closest('.ifp-wrapper').find('.ifp-img-box').addClass('show');
		}
		//jQuery('.ifp-wrapper .ifp-img .ifp-img-box').removeClass('first');
		//jQuery('.ifp-wrapper .ifp-img .ifp-img-box.show').first().addClass('first');
		if(wrap.find('.ifp-img .ifp-img-box:not(.main).show').first().length){
			wrap.find('.ifp-img .ifp-img-box:not(.main)').removeClass('active');
			wrap.find('.ifp-img .ifp-img-box:not(.main).show').first().addClass('active');
			wrap.find('.ifp-img-box.main a').attr('href', wrap.find('.ifp-img .ifp-img-box:not(.main).show').first().find('a').attr('href'));
			wrap.find('.ifp-img-box.main a img').attr('src', wrap.find('.ifp-img .ifp-img-box:not(.main).show').first().find('a img').attr('src'));
			wrap.find('.ifp-img-box.main a').show();
			wrap.find('.ifp-img-box.main span').empty().text(wrap.find('.ifp-img .ifp-img-box:not(.main).active').find('span').text());
			wrap.find('.ifp-img-description').empty().text(wrap.find('.ifp-img .ifp-img-box:not(.main).active').find('span').text());
			wrap.find('.ifp-param .ifp-img-price span').text(wrap.find('.ifp-img .ifp-img-box:not(.main).active').attr('data-price'));
		} else {
			wrap.find('.ifp-img-box.main a').hide();
			wrap.find('.ifp-img-box.main span').empty();
			wrap.find('.ifp-img-description').empty();
			wrap.find('.ifp-param .ifp-img-price span').empty();
		}
	});
	//jQuery('.ifp-wrapper .ifp-img-box a').unbind('click');
	jQuery('body').on('click', '.ifp-wrapper .ifp-img-box:not(.main) a', function(){
		var wrap = jQuery(this).closest('.ifp-wrapper');
		wrap.find('.ifp-img-box:not(.main)').removeClass('active');
		jQuery(this).closest('.ifp-img-box').addClass('active');
		wrap.find('.ifp-img-description').text(jQuery(this).closest('.ifp-img-box').find('span').text());
		wrap.find('.ifp-img-box.main').attr('data-img-id', jQuery(this).closest('.ifp-img-box').attr('data-img-id'));
		wrap.find('.ifp-img-box.main').attr('data-param-id', jQuery(this).closest('.ifp-img-box').attr('data-param-id'));
		wrap.find('.ifp-img-box.main').attr('data-price', jQuery(this).closest('.ifp-img-box').attr('data-price'));
		wrap.find('.ifp-img-box.main a').attr('href', jQuery(this).attr('href'));
		wrap.find('.ifp-img-box.main a img').attr('src', jQuery(this).find('img').attr('src'));
		wrap.find('.ifp-img-box.main span').empty().text(jQuery(this).closest('.ifp-img-box').find('span').text());
		wrap.find('.ifp-param .ifp-img-price span').text(jQuery(this).closest('.ifp-img-box').attr('data-price'));
		return false;
	});
	jQuery('body').on('click', '.ifp-wrapper .ifp-img-box.main a', function(){
		var html = '';
		html += '<div id="ifp-img-preview" data-id="'+jQuery(this).closest('.ifp-wrapper').attr('data-id')+'">';
		html += '<span class="close"></span>';
		/*html += '<div class="prev"><span></span></div>';
		html += '<div class="next"><span></span></div>';*/
		html += '<div class="ifp-img-photo" data-id="'+jQuery(this).closest('.ifp-img-box').attr('data-img-id')+'">';
		html += '<img src="'+jQuery(this).attr('href')+'" alt="" />';
		if(jQuery(this).closest('.ifp-img-box').find('span').text().length){
			html += '<br/><span class="description">'+jQuery(this).closest('.ifp-img-box').find('span').text()+'</span>';
		}
		html += '</div>';
		html += '</div>';
		jQuery('body').append(html);
		return false;
	});
	jQuery('body').on('click', '#ifp-img-preview span.close', function(){
		jQuery('#ifp-img-preview').remove();
	});
	jQuery('body').on('keyup', function(e){
		if(e.keyCode == 27) jQuery('#ifp-img-preview span.close').trigger('click');
		/*if(e.keyCode == 37) jQuery('#ifp-img-preview .prev').trigger('click');
		if(e.keyCode == 39) jQuery('#ifp-img-preview .next').trigger('click');*/
		if(e.keyCode == 37) ifpImgPrev();
		if(e.keyCode == 39) ifpImgNext();
	});
	jQuery('body').on('click', '#ifp-img-preview .ifp-img-photo', function(e){
		jQuery('#ifp-img-preview').remove();
	});
	jQuery('body').on('click', '#ifp-img-preview .ifp-img-photo img', function(e){
		var pWidth = jQuery(this).innerWidth();
		var pOffset = jQuery(this).offset(); 
		var x = e.pageX - pOffset.left;
		if(pWidth/2 > x){
		    ifpImgPrev();
		} else {
		    ifpImgNext();
		}
		e.stopPropagation();
	});
	/*jQuery('body').on('click', '#ifp-img-preview .prev', function(){
		ifpImgPrev();
	});
	jQuery('body').on('click', '#ifp-img-preview .next', function(){
		ifpImgNext();
	});*/
	jQuery('body').on('click', '.ifp-wrapper .ifp-img-buy a', function(){
		var wrapperBox = jQuery(this).closest('.ifp-wrapper');
		wrapperBox.find('.ifp-form form input[name=ifp-img-id]').val(wrapperBox.find('.ifp-img-box.main').attr('data-img-id'));
		wrapperBox.find('.ifp-form form input[name=ifp-img-param-id]').val(wrapperBox.find('.ifp-img-box.main').attr('data-param-id'));
		wrapperBox.find('.ifp-form form input[name=ifp-img-price]').val(wrapperBox.find('.ifp-img-box.main').attr('data-price'));
		wrapperBox.find('.ifp-form form input[name=ifp-img-url]').val(wrapperBox.find('.ifp-img-box.main img').attr('src'));
		wrapperBox.find('.ifp-form form input[name=ifp-img-description]').val(wrapperBox.find('.ifp-img-box.main span').text());
<<<<<<< HEAD
		wrapperBox.find('.ifp-form form input[name=ifp-page-url]').val(location.href);
=======
>>>>>>> 505167756a20d751311abe5c38a643692979dc43
		wrapperBox.find('.ifp-form form .overlay-box-img').empty().append('<img src="'+wrapperBox.find('.ifp-img-box.main img').attr('src')+'" alt="" />');
		wrapperBox.find('.ifp-form .overlay').addClass('show');
		return false;
	});
	jQuery('body').on('click', '.overlay-close', function(){
		jQuery(this).closest('.overlay').removeClass('show');
		return false;
	});
	jQuery('body').on('click', '.ifp-wrapper .ifp-form form input[name=send]', function(){
		var form = jQuery(this).closest('form');
		var name = form.find('input[name=user-name]').val();
		var phone = form.find('input[name=user-phone]').val();
		var email = form.find('input[name=user-email]').val();
		if(name.length < 2 || phone.length < 5){
			alert('Введите имя и телефон');
			return false;
		}
		var ifpFormField = [];
		form.find('input[name^=ifp-form-field-]').each(function(){
			ifpFormField.push([jQuery(this).attr('data-field'), jQuery(this).val()]);
		});
		jQuery.ajax({
			type: "POST",
			url: window.wpData.ajaxUrl,
			data:{
				action: 'ajax_ifp_img_buy',
				user_name: name,
				user_phone: phone,
				user_email: email,
				ifp_img_id: form.find('input[name=ifp-img-id]').val(),
				ifp_img_param_id: form.find('input[name=ifp-img-param-id]').val(),
				ifp_img_price: form.find('input[name=ifp-img-price]').val(),
				ifp_img_url: form.find('input[name=ifp-img-url]').val(),
				ifp_img_description: form.find('input[name=ifp-img-description]').val(),
				ifp_page_url: form.find('input[name=ifp-page-url]').val(),
				ifp_form_field: JSON.stringify(ifpFormField)
			},
			beforeSend: function(){},
			success: function(response){
				response = JSON.parse(response);
				if(response.error != 0){
					alert(response.msg);
					return false;
				}
				form.find('input[type=text]').val('');
				form.closest('.overlay').removeClass('show');
				alert(response.msg);
			},
			complete: function(){}
		});
		return false;
	});
});

jQuery(window).resize(function(){
	if(jQuery('.ifp-wrapper').width() <= 767 && jQuery('.ifp-wrapper').width() > 600){
		jQuery('.ifp-wrapper').attr('data-type', 'medium');
		jQuery('.ifp-wrapper .ifp-img-box-items > div').css('height', jQuery('.ifp-wrapper .ifp-img-box.main').outerHeight());
	} else if(jQuery('.ifp-wrapper').width() <= 600){
		jQuery('.ifp-wrapper').attr('data-type', 'small');
		jQuery('.ifp-wrapper .ifp-img-box-items > div').css('height', 'auto');
	} else {
		jQuery('.ifp-wrapper .ifp-img-box-items > div').css('height', jQuery('.ifp-wrapper .ifp-img-box.main').outerHeight());
	}
});

jQuery(window).load(function(){
	jQuery('.ifp-wrapper a.fancybox').off('click').removeClass('fancybox');
	if(jQuery('.ifp-wrapper').width() <= 767 && jQuery('.ifp-wrapper').width() > 600){
		jQuery('.ifp-wrapper').attr('data-type', 'medium');
		jQuery('.ifp-wrapper .ifp-img-box-items > div').css('height', jQuery('.ifp-wrapper .ifp-img-box.main').outerHeight());
	} else if(jQuery('.ifp-wrapper').width() <= 600){
		jQuery('.ifp-wrapper').attr('data-type', 'small');
		jQuery('.ifp-wrapper .ifp-img-box-items > div').css('height', 'auto');
	} else {
		jQuery('.ifp-wrapper .ifp-img-box-items > div').css('height', jQuery('.ifp-wrapper .ifp-img-box.main').outerHeight());
	}
});