<?php
/*
Plugin Name: Image Filter Param
Description: Промоблок, фото-фильтр
Version: 1.0
Author: koder-kim
License: GPL2

Copyright 2018  koder-kim  (email : koder.kim@gmail.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License, version 2, as 
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

if ( !defined('ABSPATH') ) {
	exit;
}

/***************************************************************
 * Install and uninstall
 ***************************************************************/


/**
 * Hooks for uninstall
 */
if (function_exists('register_uninstall_hook')) {
	register_uninstall_hook(__FILE__, 'wp_ifp_uninstall');
}


/**
 * Hooks for install
 */
if ( function_exists('register_activation_hook')) {
	register_activation_hook(__FILE__, 'wp_ifp_install');
}


/**
 * Install this plugin
 */
function wp_ifp_install() {
	global $wpdb;

	$wpdb->query("
		CREATE TABLE IF NOT EXISTS `".$wpdb->prefix."ifp_post` (
			`id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
			`name` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
			`author_id` BIGINT(20) UNSIGNED NOT NULL,
			`post_date` DATETIME NOT NULL,
			PRIMARY KEY (`id`)
		) ENGINE = InnoDB;
	");

	$wpdb->query("
		CREATE TABLE IF NOT EXISTS `".$wpdb->prefix."ifp_post_img` (
			`id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
			`img_id` BIGINT(20) UNSIGNED NOT NULL,
			`post_id` BIGINT(20) UNSIGNED NOT NULL,
			`price` FLOAT NOT NULL,
			`param` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
			`description` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
			`date` DATETIME NOT NULL,
			`sort` TINYINT(4) NOT NULL DEFAULT '0',
			PRIMARY KEY (`id`)
		) ENGINE = InnoDB;
	");

	$wpdb->query("
		CREATE TABLE IF NOT EXISTS `".$wpdb->prefix."ifp_post_param_name` (
			`id` BIGINT(20) NOT NULL AUTO_INCREMENT,
			`name` VARCHAR(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
			PRIMARY KEY (`id`)
		) ENGINE = InnoDB;
	");

	$wpdb->query("
		CREATE TABLE IF NOT EXISTS `".$wpdb->prefix."ifp_post_param` (
			`id` BIGINT(20) NOT NULL AUTO_INCREMENT,
			`param_id` BIGINT(20) UNSIGNED NOT NULL,
			`value` VARCHAR(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
			PRIMARY KEY (`id`)
		) ENGINE = InnoDB;
	");

	/*$wptuts_options = get_option('theme_wptuts_options');
    if(false === $wptuts_options){
        $wptuts_options = wptuts_get_default_options();
        add_option('theme_wptuts_options', $wptuts_options);
    }*/
}

/**
 * Uninstall this plugin
 */
function wp_ifp_uninstall() {
	global $wpdb;

	$wpdb->query("DROP TABLE `".$wpdb->prefix."ifp_post`");
	$wpdb->query("DROP TABLE `".$wpdb->prefix."ifp_post_img`");
	$wpdb->query("DROP TABLE `".$wpdb->prefix."ifp_post_param_name`");
	$wpdb->query("DROP TABLE `".$wpdb->prefix."ifp_post_param`");
}

/***************************************************************
 * END Install and uninstall
 ***************************************************************/

// Стрнаица настроек
function wp_ifp_add_options_page(){
	if(function_exists('add_options_page')){
		$page_title = __('WP Image Filter Param', 'wp_ifp_page');
		$menu_title = __('WP Image Filter Param', 'wp_ifp_page');
		$capability = 'administrator';
		$menu_slug = 'wp_ifp_page';
		$function = 'wp_ifp_setting_page';
		add_options_page($page_title, $menu_title, $capability, $menu_slug, $function);
	}
}
add_action('admin_menu', 'wp_ifp_add_options_page');

function wp_ifp_setting_page() {
	$path = trailingslashit(dirname(__FILE__));
	if (!file_exists($path.'settings.php')) {
		return false;
	}
	require_once($path.'settings.php');
}

// Ссылка на страницу настроек
function wp_ifp_plugin_row_meta($links, $file) {
	if ($file == plugin_basename(__FILE__)) {
		$settings_page = 'wp_ifp_page';
		$links[] = '<a href="options-general.php?page=' . $settings_page .'">' . __('Settings','wp_ifp_page') . '</a>';
	}
	return $links;
}
add_filter('plugin_row_meta', 'wp_ifp_plugin_row_meta',10,2);

/**
 * Manage the option when we submit the form
 */
function wp_ifp_save_settings(){
	/*register_setting( 'wp-sitemap-page', 'wsp_posts_by_category' );
	register_setting( 'wp-sitemap-page', 'wsp_exclude_pages' );*/
} 
add_action('admin_init', 'wp_ifp_save_settings');

function wptuts_options_enqueue_scripts(){
	wp_enqueue_script("jquery-ui-core", array('jquery'));
	wp_enqueue_script("jquery-ui-sortable", array('jquery','jquery-ui-core'));
	wp_enqueue_style('ifp-style', plugins_url('/css/admin-settings.css', __FILE__));
	wp_enqueue_script('ifp-script', plugins_url('/js/admin-settings.js', __FILE__));
    wp_enqueue_media();

    $langs = array(
    	'Add' => __('Add', 'wp-img-filter-param'),
    	'Cancel' => __('Cancel', 'wp-img-filter-param'),
    	'Add_param' => __('Add param', 'wp-img-filter-param'),
    	'Price' => __('Price', 'wp-img-filter-param')
    );
  	wp_localize_script('ifp-script', 'langs', $langs);
}
add_action('admin_enqueue_scripts', 'wptuts_options_enqueue_scripts');

/* ************* AJAX ************ */

// *********** DB ifp_param_name ***********
add_action('wp_ajax_ifp_param_name_get', 'ifp_db_param_name_get');
function ifp_db_param_name_get(){
	global $wpdb;
	$query = "SELECT * FROM `".$wpdb->prefix."ifp_post_param_name`";
	$param_name = $wpdb->get_results($query);
	$response = '';
	foreach($param_name as $row):
		$response .= '<option value="'.$row->id.'">'.$row->name.'</option>';
	endforeach;
	echo $response;
	wp_die();
}

add_action('wp_ajax_ifp_param_name_add', 'ifp_db_param_name_add');
function ifp_db_param_name_add(){
	global $wpdb;

	$name = $_POST['name'];
	$query = "SELECT * FROM `".$wpdb->prefix."ifp_post_param_name` WHERE `name` LIKE '".$name."'";
	$param_name = $wpdb->get_row($query, ARRAY_A);
	if(count($param_name) == 0){
		echo $wpdb->insert($wpdb->prefix.'ifp_post_param_name', array('name' => $name));
	} else {
		echo true;
	}

	wp_die();
}
// *********** End DB ***********

// *********** DB ifp_param ***********
add_action('wp_ajax_ifp_param_get', 'ifp_db_param_get');
function ifp_db_param_get(){
	global $wpdb;
	$query = "SELECT * FROM `".$wpdb->prefix."ifp_post_param`";
	$param_id = intval($_POST['param_id']);
	if(is_int($param_id)){
		$query .= " WHERE `param_id` = ".$param_id;
	}
	$param_name = $wpdb->get_results($query);
	$response = '';
	foreach($param_name as $row):
		$response .= '<option value="'.$row->id.'">'.$row->value.'</option>';
	endforeach;
	echo $response;
	wp_die();
}

add_action('wp_ajax_ifp_param_add', 'ifp_db_param_add');
function ifp_db_param_add(){
	global $wpdb;

	$value = $_POST['value'];
	$param_id = $_POST['param_id'];
	$query = "SELECT * FROM `".$wpdb->prefix."ifp_post_param` WHERE `param_id` = '".$param_id."' AND `value` LIKE '".$value."'";
	$param_name = $wpdb->get_row($query, ARRAY_A);
	if(count($param_name) == 0){
		echo $wpdb->insert($wpdb->prefix.'ifp_post_param', array('value' => $value, 'param_id' => $param_id));
	} else {
		echo true;
	}

	wp_die();
}
// *********** End DB ***********

// *********** DB ifp_post_img ***********
add_action('wp_ajax_ifp_post_img_get', 'ifp_db_post_img_get');
function ifp_db_post_img_get(){
	global $wpdb;
	
	$post_id = intval($_POST['post_id']);
	$echo = isset($_POST['echo']) ? $_POST['echo'] : false;
	$query = "SELECT * FROM `".$wpdb->prefix."ifp_post_img`";
	if(is_int($post_id)){
		$query .= " WHERE `post_id` = ".$post_id;
	}
	$query .= " ORDER BY `sort` ASC";
	$ifp_post_img = $wpdb->get_results($query);
	if($echo){
		$response = array();
		foreach($ifp_post_img as $row):
			$params = array();
			$params['id'] = $row->id;
			$params['img_id'] = $row->img_id;
			$params['img'] = wp_get_attachment_image($row->img_id, 'thumbnail');
			$params['price'] = $row->price;
			if($row->param){
				$query = "SELECT * FROM `".$wpdb->prefix."ifp_post_param` WHERE `id` IN (".implode(',',json_decode($row->param)).")";
				$params['params'] = $wpdb->get_results($query);
			} else {
				$params['params'] = '';
			}
			$params['description'] = $row->description;
			$response[] = $params;
		endforeach;
		echo json_encode($response);
	} else {
		echo json_decode($ifp_post_img);
	}
	wp_die();
}

add_action('wp_ajax_ifp_post_img_add', 'ifp_db_post_img_add');
function ifp_db_post_img_add(){
	global $wpdb;

	$post_id = $_POST['post_id'];
	$items = $_POST['items'];
	$query = "INSERT INTO `".$wpdb->prefix."ifp_post_img` (`img_id`, `post_id`, `date`) VALUES ";
	$separator = false;
	foreach(json_decode($items) as $item){
		$query .= $separator ? ',' : '';
		$query .= "(".$item.",".$post_id.",'".date('Y-m-d H:i:s')."')";
		$separator = true;
	}
	$result = $wpdb->query($wpdb->prepare($query));
	if($result !== FALSE){
		$result = $wpdb->insert_id;
	}
	echo $result;
	
	wp_die();
}

add_action('wp_ajax_ifp_post_img_update', 'ifp_db_post_img_update');
function ifp_db_post_img_update(){
	global $wpdb;

	$id = $_POST['id'];
	$param = $_POST['param'];
	$result = $wpdb->update($wpdb->prefix."ifp_post_img", array('param' => json_encode($param), 'date' => date('Y-m-d H:i:s')), array('id' => $id));
	echo $result;
	
	wp_die();
}

add_action('wp_ajax_ifp_post_img_update_description', 'ifp_db_post_img_update_description');
function ifp_db_post_img_update_description(){
	global $wpdb;

	$id = $_POST['id'];
	$description = $_POST['description'];
	$result = $wpdb->update($wpdb->prefix."ifp_post_img", array('description' => $description, 'date' => date('Y-m-d H:i:s')), array('id' => $id));
	echo $result;
	
	wp_die();
}

add_action('wp_ajax_ifp_post_img_update_price', 'ifp_db_post_img_update_price');
function ifp_db_post_img_update_price(){
	global $wpdb;

	$id = $_POST['id'];
	$price = $_POST['price'];
	$result = $wpdb->update($wpdb->prefix."ifp_post_img", array('price' => $price, 'date' => date('Y-m-d H:i:s')), array('id' => $id));
	echo $result;
	
	wp_die();
}

add_action('wp_ajax_ifp_post_img_delete', 'ifp_db_post_img_delete');
function ifp_db_post_img_delete(){
	global $wpdb;

	$id = $_POST['id'];
	$wpdb->delete($wpdb->prefix."ifp_post_img", array('id' => $id));

	wp_die();
}
// *********** End DB ***********

// *********** DB ifp_post ***********
add_action('wp_ajax_ifp_post_get', 'ifp_db_post_get');
function ifp_db_post_get(){
	global $wpdb;
	$query = "SELECT * FROM `".$wpdb->prefix."ifp_post`";
	$id = intval($_POST['id']);
	if(is_int($param_id)){
		$query .= " WHERE `id` = ".$id;
	}
	$ifp_post = $wpdb->get_results($query);
	$response = json_encode($ifp_post);
	echo $response;
	wp_die();
}

add_action('wp_ajax_ifp_post_add', 'ifp_db_post_add');
function ifp_db_post_add(){
	global $wpdb;

	$id = $_POST['id'];
	$name = $_POST['name'];
	if($id && $id != 0){
		$result = $wpdb->update($wpdb->prefix."ifp_post", array('name' => $name, 'author_id' => get_current_user_id(), 'post_date' => date('Y-m-d H:i:s')), array('id' => $id));
	} else {
		$result = $wpdb->insert($wpdb->prefix."ifp_post", array('name' => $name, 'author_id' => get_current_user_id(), 'post_date' => date('Y-m-d H:i:s')), array('%s', '%d', '%s'));
	}
	echo $result;

	wp_die();
}

add_action('wp_ajax_ifp_post_delete', 'ifp_db_post_delete');
function ifp_db_post_delete(){
	global $wpdb;

	$id = $_POST['id'];
	$wpdb->delete($wpdb->prefix."ifp_post_img", array('post_id' => $id));
	$wpdb->delete($wpdb->prefix."ifp_post", array('id' => $id));

	wp_die();
}
// *********** End DB ***********

// WP admin-ajax.php
function jsAjaxUrl(){
  $vars = array(
    'ajaxUrl' => admin_url('admin-ajax.php'),
    'is_mobile' => wp_is_mobile()
  );
  echo '<script type="text/javascript">window.wpData = '.json_encode($vars).';</script>';
}
add_action('wp_head', 'jsAjaxUrl');

add_shortcode('wp_ifp', 'wp_ifp_post');
function wp_ifp_post($atts){
	global $wpdb, $post;
	if(!isset($atts['id'])){
		return 'wp_ifp id not found';
	}

	wp_enqueue_style('wp_ifp_post_css', plugins_url('/css/ifp.css', __FILE__));
	wp_enqueue_script('wp_ifp_post_js', plugins_url('/js/ifp.js', __FILE__));

	$param_name = $wpdb->get_results("SELECT * FROM `".$wpdb->prefix."ifp_post_param_name`");
	$query = "SELECT * FROM `".$wpdb->prefix."ifp_post_img` WHERE `post_id` = ".intval($atts['id'])." ORDER BY `sort` ASC";
	$list = $wpdb->get_results($query);
	
	$html = '';
	$paramsId = array();
	ob_start();
	$main = true;
	$mainImg = current($list);
?>
	<div class="ifp-wrapper" data-id="<?php echo $atts['id']; ?>">
		<div class="ifp-img">
			<div class="ifp-img-box main" data-param-id="<?php echo $mainImg->param; ?>" data-img-id="<?php echo $mainImg->img_id; ?>" data-price="<?php echo $mainImg->price; ?>">
				<a href="<?php echo wp_get_attachment_image_url($mainImg->img_id, 'full'); ?>">
					<?php echo wp_get_attachment_image($mainImg->img_id, 'large'); ?>
				</a>
				<span><?php echo $mainImg->description; ?></span>
			</div>
			<div class="ifp-img-box-items">
				<div>
<?php
	foreach($list as $row):
?>
				<div class="ifp-img-box show" data-param-id='<?php echo $row->param; ?>' data-img-id="<?php echo $row->img_id; ?>" data-price="<?php echo $row->price; ?>">
					<a href="<?php echo wp_get_attachment_image_url($row->img_id, 'full'); ?>">
						<?php echo wp_get_attachment_image($row->img_id, 'large'); ?>
					</a>
					<span><?php echo $row->description; ?></span>
				</div>
<?php
		if($row->param){
			$paramsId = array_merge($paramsId, json_decode($row->param));
		}
		$main = false;
	endforeach; ?>
				</div>
			</div>
		</div>
		<div class="ifp-param">
			<div class="ifp-img-buy">
				<a href="#"><?php _e('Buy', 'wp-img-filter-param'); ?></a>
			</div>
			<div class="ifp-img-description"><?php echo $mainImg->description; ?></div>
			<div class="ifp-img-price"><?php _e('Price', 'wp-img-filter-param'); ?>: <span><?php echo $mainImg->price; ?></span></div>
<?php
	$query = "SELECT * FROM `".$wpdb->prefix."ifp_post_param` WHERE `id` IN (".implode(',', $paramsId).")";
	$params = $wpdb->get_results($query);
	foreach($param_name as $item){
		$html = '';
		foreach($params as $row):
			if($row->param_id == $item->id):
				$html .= '<span data-id="'.$row->id.'">'.$row->value.'</span>';
			endif;
		endforeach;
		if($html != ''){
?>
			<div class="ifp-param-box" data-id="<?php echo $item->id; ?>">
				<?php echo $item->name; ?>:
				<?php echo $html; ?>
			</div>
<?php
		}
	}
?>
		</div>
		<div class="ifp-form">
			<div class="overlay">
				<div class="overlay-box">
					<div class="overlay-close">&times;</div>
					<form action="">
						<input type="hidden" name="ifp-img-id">
						<input type="hidden" name="ifp-img-param-id">
						<input type="hidden" name="ifp-img-price">
						<input type="hidden" name="ifp-img-url">
						<input type="hidden" name="ifp-img-description">
<<<<<<< HEAD
						<input type="hidden" name="ifp-page-url">
=======
>>>>>>> 505167756a20d751311abe5c38a643692979dc43
						<div class="overlay-box-img"></div>
						<input type="text" name="user-name" placeholder="<?php _e('Name', 'wp-img-filter-param'); ?>">
						<input type="text" name="user-phone" placeholder="<?php _e('Phone', 'wp-img-filter-param'); ?>">
						<input type="text" name="user-email" placeholder="<?php _e('E-mail', 'wp-img-filter-param'); ?>">
						<?php
							if(function_exists('get_field_object')){
								$field = get_field_object('ifp_form_field', $post->ID);
								if($field['type'] == 'checkbox'):
									foreach($field['value'] as $key => $val){
										echo '<input type="text" name="ifp-form-field-'.$key.'" data-field="'.$val.'" placeholder="'.$val.'">';
									}
								endif;
							}
						?>
						<input type="button" name="send" value="<?php _e('Send', 'wp-img-filter-param'); ?>">
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="ifp-information">
		<div><span>Оплатить можно после отчета</span></div>
		<div><span>2 года гарантии</span></div>
		<div><span>Толстая экокожа и толстый поролон</span></div>
		<div><span>Специально под (модель авто)</span></div>
		<div><span>Все клиенты остаются довольны</span></div>
	</div>
<?php
	$html = ob_get_contents();
	ob_end_clean();
	return $html;
}

add_action('wp_ajax_ajax_ifp_img_buy', 'ifp_img_buy');
add_action('wp_ajax_nopriv_ajax_ifp_img_buy', 'ifp_img_buy');
function ifp_img_buy(){
	$user_name = $_POST['user_name'];
	$user_phone = $_POST['user_phone'];
	$user_email = $_POST['user_email'];
	$ifp_img_id = $_POST['ifp_img_id'];
	$ifp_img_param_id = $_POST['ifp_img_param_id'];
	$ifp_img_price = $_POST['ifp_img_price'];
	$path = $_POST['ifp_img_url'];
	$ifp_img_description = $_POST['ifp_img_description'];
	$ifp_page_url = $_POST['ifp_page_url'];
	$ifp_form_field = $_POST['ifp_form_field'];


	$mail_to = 'koder.kim@gmail.com';
	$thema = 'Заказ';
	ob_start();
?>
	<div>
		<p>Url: <?php echo $ifp_page_url; ?></p>
		<p><?php _e('User', 'wp-img-filter-param') ?>: <?php echo $user_name; ?></p>
		<p><?php _e('Phone', 'wp-img-filter-param'); ?>: <?php echo $user_phone; ?></p>
		<p>E-mail: <?php echo $user_email; ?></p>
		<br>
		<p>Дополнительные данные:</p>
		<table>
			<thead>
				<tr>
					<th><?php _e('ID', 'wp-img-filter-param'); ?></th>
					<th><?php _e('Param', 'wp-img-filter-param'); ?></th>
					<th><?php _e('Price', 'wp-img-filter-param'); ?></th>
					<th><?php _e('Description', 'wp-img-filter-param'); ?></th>
					<th><?php _e('Additional', 'wp-img-filter-param'); ?></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><?php echo $ifp_img_id; ?></td>
					<td><?php echo $ifp_img_param_id; ?></td>
					<td><?php echo $ifp_img_price; ?></td>
					<td><?php echo $ifp_img_description; ?></td>
					<td>
					<?php
						if($ifp_form_field == '') echo '';
						else {
							$ifp_form_field = json_decode(stripslashes($ifp_form_field), true);
							foreach($ifp_form_field as $val){
								echo $val[0].': '.$val[1].'<br>';
							}
						}
					?>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
<?
	$html = ob_get_contents();
	ob_clean();

	$result = array(
		'error' => '0'
	);
	$result['msg'] = send_mail($mail_to, $thema, $html, $path);

	echo json_encode($result);

	wp_die();
}

add_action('wp_ajax_ifp_post_img_sort', 'ifp_post_img_sort');
add_action('wp_ajax_nopriv_ifp_post_img_sort', 'ifp_post_img_sort');
function ifp_post_img_sort(){
	global $wpdb;

	$sortList = json_decode(str_replace('\\', '', $_POST['sortList']));
	foreach($sortList as $key => $val){
		$wpdb->update($wpdb->prefix."ifp_post_img", array('sort' => $key), array('id' => $val));
	}

	wp_die();
}

function send_mail($mail_to, $thema, $html, $path){
	if($path && ($file = file_get_contents($path))){
		$name = explode('/', $path);
		$name = end($name);
		$EOL = "\r\n"; // ограничитель строк, некоторые почтовые сервера требуют \n - подобрать опытным путём
		$boundary   = "--".md5(uniqid(time()));
		$headers    = "MIME-Version: 1.0;$EOL";
		$headers   .= "Content-Type: multipart/mixed; boundary=\"$boundary\"$EOL";
		$headers   .= "From: order@".$_SERVER['HTTP_HOST'];
		  
		$multipart  = "--$boundary$EOL";
		$multipart .= "Content-Type: text/html; charset=utf-8$EOL";
		$multipart .= "Content-Transfer-Encoding: base64$EOL";
		$multipart .= $EOL;
		$multipart .= chunk_split(base64_encode($html));
		$multipart .=  "$EOL--$boundary$EOL";
		$multipart .= "Content-Type: application/octet-stream; name=\"$name\"$EOL";
		$multipart .= "Content-Transfer-Encoding: base64$EOL";
		$multipart .= "Content-Disposition: attachment; filename=\"$name\"$EOL";
		$multipart .= $EOL;
		$multipart .= chunk_split(base64_encode($file));
		$multipart .= "$EOL--$boundary--$EOL";

		if(!mail($mail_to, $thema, $multipart, $headers)){
			return 'Не удалось отправить письмо!';
		} else {
			return 'Письмо успешно отправлено!';
		}
	} else {
		if(!mail($mail_to, $thema, $html)){
			return 'Не удалось отправить письмо!';
		} else {
			return 'Письмо успешно отправлено!';
		}
	}
}
?>